// “Las tijeras cortan el papel, 
// el papel cubre a la piedra, 
// la piedra aplasta al lagarto, 
// el lagarto envenena a Spock, 
// Spock destroza las tijeras, 
// las tijeras decapitan al lagarto, 
// el lagarto se come el papel, 
// el papel refuta a Spock, 
// Spock vaporiza la piedra, 
// y, como es habitual… 
// la piedra aplasta las tijeras.”

function aleatorio(minimo,maximo)
{
	var numero = Math.floor( Math.random() * (maximo - minimo + 1) + minimo );
	return numero;
}

var piedra = 0;
var papel = 1;
var tijera = 2;
var lagarto = 3;
var spock = 4;

var opciones = ["Piedra","Papel","Tijera","Lagarto","Spock"];

var opcionUsuario;
var opcionJavascript=aleatorio(0,4);

opcionUsuario = prompt("¿Qué eliges?\nPiedra:0\nPapel:1\nTijera:2\nLagarto:3\nSpock:4",0);

alert("Elegiste " + opciones[opcionUsuario]);
alert("Javascript Eligió " + opciones[opcionJavascript]);

if (opcionUsuario == piedra) 
{
	if (opcionJavascript == piedra) 
	{
		alert("Empate!");
	}
	else if (opcionJavascript == papel) 
	{
		alert("Perdiste! te comio el papel");
	}
	else if (opcionJavascript == tijera) 
	{
		alert("Ganaste! aplastaste la tijera");
	}
	else if (opcionJavascript == lagarto)
	{
		alert("Ganaste! murio el lagarto");
	}
	else if (opcionJavascript == spock) 
	{
		alert("Perdiste! te vaporizarón");
	}
}

else if (opcionUsuario == papel) 
{
	if (opcionJavascript == piedra) 
	{
		alert("Ganaste! cubriste la piedra");
	}
	else if (opcionJavascript == papel) 
	{
		alert("Empate!");
	}
	else if (opcionJavascript == tijera) 
	{
		alert("Perdiste! te hicieron añicos");
	}
	else if (opcionJavascript == lagarto)
	{
		alert("Perdiste! estas en la pansa del lagarto");
	}
	else if (opcionJavascript == spock) 
	{
		alert("Ganaste!");
	}
}
else if (opcionUsuario == tijera) 
{
	if (opcionJavascript == piedra) 
	{
		alert("Perdiste! quedaste aplastado");
	}
	else if (opcionJavascript == papel) 
	{
		alert("Ganaste! lo cortaste por completo");
	}
	else if (opcionJavascript == tijera) 
	{
		alert("Empate!");
	}
	else if (opcionJavascript == lagarto)
	{
		alert("Ganaste! solo quedo su cabeza");
	}
	else if (opcionJavascript == spock) 
	{
		alert("Perdiste! te destrozarón");
	}
}
else if (opcionUsuario == lagarto) 
{
	if (opcionJavascript == piedra) 
	{
		alert("Perdiste! quedaste aplastado");
	}
	else if (opcionJavascript == papel) 
	{
		alert("Ganaste! te comiste el papel");
	}
	else if (opcionJavascript == tijera) 
	{
		alert("Perdiste! te decapitarón");
	}
	else if (opcionJavascript == lagarto)
	{
		alert("Empate!");
	}
	else if (opcionJavascript == spock) 
	{
		alert("Ganaste, lo has envenenado");
	}
}
else if (opcionUsuario == spock) 
{
	if (opcionJavascript == piedra) 
	{
		alert("Ganaste! quedo vaporizado");
	}
	else if (opcionJavascript == papel) 
	{
		alert("Perdiste!");
	}
	else if (opcionJavascript == tijera) 
	{
		alert("Ganaste! lo has destrozado");
	}
	else if (opcionJavascript == lagarto)
	{
		alert("Perdiste, te enveneron");
	}
	else if (opcionJavascript == spock) 
	{
		alert("Empate!");
	}
}
else 
{
	alert("Pero que carajo!");
}
